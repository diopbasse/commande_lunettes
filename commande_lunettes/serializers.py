from rest_framework import serializers
from commande_lunettes.models import Client,Commande,Lunettes


class ClientSerializer(serializers.ModelSerializer):
	class Meta:
		model = Client
		fields = '__all__'


class LunettesSerializer(serializers.ModelSerializer):

	class Meta:
		model = Lunettes
		fields = '__all__'

class CommandeSerializer(serializers.ModelSerializer):

	class Meta:
		model = Commande
		fields = '__all__'