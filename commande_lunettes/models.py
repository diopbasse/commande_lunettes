from django.db import models

# Create your models here.
class Client(models.Model):
	"""docstring for Client"""
	nom = models.CharField(max_length=30, blank=False, null=False)
	prenom = models.CharField(max_length=30, blank=False, null=False)
	adresse = models.TextField(max_length=1000, blank=False, null=False)
	telephone = models.CharField(max_length=1000, blank=False, null=False)
	photo = models.FileField(blank=True, null=True)


	def __str__(self):
		return str(self.nom)+ ''


		

class Lunettes(models.Model):
	"""docstring for lunettes"""
	nom = models.CharField(max_length=30, blank=False, null=False)
	types = models.CharField(max_length=30, blank=False, null=False)
	prix = models. FloatField(blank=False, null=False)
	photo = models.FileField(blank=True, null=True)
	

	def __str__(self):
		return str(self.nom)+ ''




class Commande(models.Model):
	"""docstring for commande"""
	Client = models.ForeignKey('Client',on_delete=models.CASCADE, null=True,blank=True )
	Lunettes = models.ForeignKey('Lunettes',on_delete=models.CASCADE, null=True,blank=True )
	nbre = models.IntegerField(default=0)
	montant_total = models. FloatField(blank=False, null=False)
	date_com = models.DateTimeField('date de la commande')

	def __str__(self):
		return str(self.nbre)+ ''
