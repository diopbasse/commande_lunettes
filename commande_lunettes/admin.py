from django.contrib import admin
from commande_lunettes.models import Client,Lunettes,Commande
# Register your models here.
admin.site.register(Client)
admin.site.register(Lunettes)
admin.site.register(Commande)