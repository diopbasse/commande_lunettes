from django.shortcuts import render
from django.http import HttpResponse
from rest_framework import generics, permissions, status
from commande_lunettes.models import *
from commande_lunettes.serializers import *
from rest_framework.response import Response

# Create your views here.
def index(request):

	return HttpResponse( 'Bienvenue dans la gestion des commande des lunettes')


class ClientListCreateView(generics.CreateAPIView):
	permission_classes = (
		permissions.IsAuthenticated,
	)
	serializer_class = ClientSerializer
	def get(self, request, *args, **kwargs):
		client = Client.objects.all()

		if not client:
			return Response({
				"status": "failure",
				"message": "no such item."
			}, status=status.HTTP_404_NOT_FOUND)

		serializer = ClientSerializer(client, many=True)


		return Response({
			"status":"success",
			"message":"items successfully retrieved.",
			"count" : client.count(),
			"data": serializer.data
		}, status=status.HTTP_200_OK)

	def post(self, request, *args, **kwargs):
		serializer = ClientSerializer(data=request.data)
		if not serializer.is_valid():
			return Response({
				"status":"failure",
				"message": serializer.errors,
				"error":"erreur"
			}, status=status.HTTP_400_BAD_REQUEST)

		serializer.save()
		return Response({
			"status": "success",
			"message":"item successfully created",
		}, status=status.HTTP_201_CREATED)


# update client
class ClientUpdateView(generics.CreateAPIView):
	serializer_class = ClientSerializer
	def put(self, request, *args, **kwargs):
		try:
			client = Client.objects.get(id=kwargs['pk'])
		except Client.DoesNotExist:
			return Response({
				"status":"failure",
				"message":"no such item"
			}, status=status.HTTP_400_BAD_REQUEST)

		client_data = {
			'nom': request.data['nom'],
			'prenom': request.data['prenom'],
			'adresse': request.data['adresse'],
			'telephone': request.data['telephone'],
		}

		serializer = ClientSerializer(client, data=client_data, partial=True)

		if not serializer.is_valid():
			return Response({
				"status":"failure",
				"message": "invalid data",
				"errors": serializer.errors
			}, status=status.HTTP_400_BAD_REQUEST)

		serializer.save()
		return Response({
			"status": "success",
			"message": "item successfully update",
			"data": serializer.data
		}, status=status.HTTP_200_OK)


# delete client
class ClientDeleteView(generics.CreateAPIView):

	serializer_class = ClientSerializer
	def post(self, request, *args, **kwargs):
		try:
			client = Client.objects.get(id=kwargs['pk'])
		except Client.DoesNotExist:
			return Response({
				"status":"failure",
				"message": "no such item"
			}, status=status.HTTP_400_BAD_REQUEST)


		client.delete()
		return Response({
			"status": "success",
			"message": "client successfully deleted"
		}, status=status.HTTP_200_OK)


		# views for lunettes
class LunettesListCreateView(generics.CreateAPIView):
	permission_classes = (
		permissions.IsAuthenticated,
	)
	serializer_class = LunettesSerializer
	def get(self, request, *args, **kwargs):
		lunettes = Lunettes.objects.all()

		if not lunettes:
			return Response({
				"status": "failure",
				"message": "no such item."
			}, status=status.HTTP_404_NOT_FOUND)

		serializer = LunettesSerializer(lunettes, many=True)


		return Response({
			"status":"success",
			"message":"items successfully retrieved.",
			"count" : lunettes.count(),
			"data": serializer.data
		}, status=status.HTTP_200_OK)

	def post(self, request, *args, **kwargs):
		serializer = LunettesSerializer(data=request.data)
		if not serializer.is_valid():
			return Response({
				"status":"failure",
				"message": serializer.errors,
				"error":"erreur"
			}, status=status.HTTP_400_BAD_REQUEST)

		serializer.save()
		return Response({
			"status": "success",
			"message":"item successfully created",
		}, status=status.HTTP_201_CREATED)

#class for update lunettes
class LunettesUpdateView(generics.CreateAPIView):
	serializer_class = LunettesSerializer
	def put(self, request, *args, **kwargs):
		try:
			lunettes = Lunettes.objects.get(id=kwargs['pk'])
		except Lunettes.DoesNotExist:
			return Response({
				"status":"failure",
				"message":"no such item"
			}, status=status.HTTP_400_BAD_REQUEST)

		lunettes_data = {
			'nom': request.data['nom'],
			'types': request.data['types'],
			'prix': request.data['prix'],
			'photo': request.data['photo'],
		}

		serializer = LunettesSerializer(lunettes, data=lunettes_data, partial=True)

		if not serializer.is_valid():
			return Response({
				"status":"failure",
				"message": "invalid data",
				"errors": serializer.errors
			}, status=status.HTTP_400_BAD_REQUEST)

		serializer.save()
		return Response({
			"status": "success",
			"message": "item successfully update",
			"data": serializer.data
		}, status=status.HTTP_200_OK)


# delete lunettes
class LunettesDeleteView(generics.CreateAPIView):

	serializer_class = LunettesSerializer
	def post(self, request, *args, **kwargs):
		try:
			lunettes = lunettes.objects.get(id=kwargs['pk'])
		except lunettes.DoesNotExist:
			return Response({
				"status":"failure",
				"message": "no such item"
			}, status=status.HTTP_400_BAD_REQUEST)


		lunettes.delete()
		return Response({
			"status": "success",
			"message": "lunettes successfully deleted"
		}, status=status.HTTP_200_OK)


# class for views commandes
class CommandeListCreateView(generics.CreateAPIView):
	permission_classes = (
		permissions.IsAuthenticated,
	)
	serializer_class = CommandeSerializer
	def get(self, request, *args, **kwargs):
		commande = Commande.objects.all()

		if not commande:
			return Response({
				"status": "failure",
				"message": "no such item."
			}, status=status.HTTP_404_NOT_FOUND)

		serializer = CommandeSerializer(commande, many=True)


		return Response({
			"status":"success",
			"message":"items successfully retrieved.",
			"count" : commande.count(),
			"data": serializer.data
		}, status=status.HTTP_200_OK)

	def post(self, request, *args, **kwargs):
		serializer = CommandeSerializer(data=request.data)
		if not serializer.is_valid():
			return Response({
				"status":"failure",
				"message": serializer.errors,
				"error":"erreur"
			}, status=status.HTTP_400_BAD_REQUEST)

		serializer.save()
		return Response({
			"status": "success",
			"message":"item successfully created",
		}, status=status.HTTP_201_CREATED)


# update client
class CommandeUpdateView(generics.CreateAPIView):
	serializer_class = CommandeSerializer
	def put(self, request, *args, **kwargs):
		try:
			commande = Commande.objects.get(id=kwargs['pk'])
		except Commande.DoesNotExist:
			return Response({
				"status":"failure",
				"message":"no such item"
			}, status=status.HTTP_400_BAD_REQUEST)

		commande_data = {
			'nbre': request.data['nbre'],
			'date_com': request.data['date_com'],
			
		}

		serializer = ClientSerializer(commande, data=commande_data, partial=True)

		if not serializer.is_valid():
			return Response({
				"status":"failure",
				"message": "invalid data",
				"errors": serializer.errors
			}, status=status.HTTP_400_BAD_REQUEST)

		serializer.save()
		return Response({
			"status": "success",
			"message": "item successfully update",
			"data": serializer.data
		}, status=status.HTTP_200_OK)


# delete commande
class CommandeDeleteView(generics.CreateAPIView):

	serializer_class = CommandeSerializer
	def post(self, request, *args, **kwargs):
		try:
			commande = Commande.objects.get(id=kwargs['pk'])
		except Commande.DoesNotExist:
			return Response({
				"status":"failure",
				"message": "no such item"
			}, status=status.HTTP_400_BAD_REQUEST)


		commande.delete()
		return Response({
			"status": "success",
			"message": "commande successfully deleted"
		}, status=status.HTTP_200_OK)