from django.conf.urls import  url, include 
from django.urls import path
# from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token, verify_jwt_token

from . import views
from rest_framework import routers

router = routers.DefaultRouter()

urlpatterns = [
    path('',views.index, name='index'),
    url(r'^client/$',views.ClientListCreateView.as_view()),
    url(r'^clientupdate/(?P<pk>[0-9]+)/$',views.ClientUpdateView.as_view()),
    url(r'^clientdelete/(?P<pk>[0-9]+)/$',views.ClientDeleteView.as_view()),
    url(r'^createlunettes/$',views.LunettesListCreateView.as_view()),
    url(r'^lunettesupdate/(?P<pk>[0-9]+)/$',views.LunettesUpdateView.as_view()),
    url(r'^lunettesdelete/(?P<pk>[0-9]+)/$',views.LunettesDeleteView.as_view()),
    url(r'^commande/$', views.CommandeListCreateView.as_view()),
    url(r'commandeupdate/(?P<pk>[0-9]+)/$', views.CommandeUpdateView.as_view()),
    url(r'^commandedelete/(?P<pk>[0-9]+)/$',views.CommandeDeleteView.as_view()),
    # url(r'categorybyid/$', views.CategoryById.as_view()),
    # url(r'login/$', obtain_jwt_token)

]
