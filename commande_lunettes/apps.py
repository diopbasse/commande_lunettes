from django.apps import AppConfig


class CommandeLunettesConfig(AppConfig):
    name = 'commande_lunettes'
